#include "myfs.h"
#include <stdint.h>
#include <string.h>
#include <iostream>
#include <math.h>
#include <sstream>
#include <iomanip>

const char *MyFs::MYFS_MAGIC = "MYFS";

MyFs::MyFs(BlockDeviceSimulator *blkdevsim_) : blkdevsim(blkdevsim_)
{
	struct myfs_header header;
	blkdevsim->read(0, sizeof(header), (char *)&header);

	if (strncmp(header.magic, MYFS_MAGIC, sizeof(header.magic)) != 0 ||
		(header.version != CURR_VERSION))
	{
		std::cout << "Did not find myfs instance on blkdev" << std::endl;
		std::cout << "Creating..." << std::endl;
		format();
		std::cout << "Finished!" << std::endl;
	}
	else
	{
		blkdevsim->read(NODES_START, sizeof(this->nodes), (char *)&nodes);
	}
}

void MyFs::format()
{
	struct myfs_header header;
	strncpy(header.magic, MYFS_MAGIC, sizeof(header.magic));
	header.version = CURR_VERSION;
	blkdevsim->write(0, sizeof(header), (const char *)&header);

	memset(nodes.inodes, 0, sizeof(nodes.inodes));
	nodes.currEmptyLoc = DATA_START;
	nodes.currNode = 0;
}

void MyFs::create_file(std::string path_str, bool directory)
{
	strncpy(nodes.inodes[nodes.currNode].name, path_str.c_str(), PATH_MAX_LEN);
	nodes.inodes[nodes.currNode].addr = 0;
	nodes.inodes[nodes.currNode].size = 0;
	++nodes.currNode;
	writeNodes();
}

void MyFs::writeNodes()
{
	blkdevsim->write(NODES_START, sizeof(this->nodes), (char *)&nodes);
}

std::string MyFs::get_content(std::string path_str)
{
	for (size_t i = 0; i < INODES_COUNT; i++)
	{
		if (strcmp(nodes.inodes[i].name, path_str.c_str()) == 0) //found
		{
			if (nodes.inodes[i].addr != 0 && nodes.inodes[i].size != 0)
			{
				char *data = new char[nodes.inodes[i].size];
				this->blkdevsim->read(nodes.inodes[i].addr, nodes.inodes[i].size, data);
				std::string ans = data;
				delete data;
				return ans;
			}
			else
			{
				return "";
			}
		}
	}
	throw std::runtime_error("ERROR: path not found!");
	return "";
}

void MyFs::set_content(std::string path_str, std::string content)
{
	bool found = false;
	for (size_t i = 0; i < INODES_COUNT; i++)
	{
		if (strcmp(nodes.inodes[i].name, path_str.c_str()) == 0) //found
		{
			found = true;
			nodes.inodes[i].addr = nodes.currEmptyLoc;
			nodes.inodes[i].size = content.size() + 1;
			nodes.currEmptyLoc += content.size() + 2;
			this->blkdevsim->write(nodes.inodes[i].addr, nodes.inodes[i].size, content.c_str());
			writeNodes();
		}
	}
	if (!found)
		throw std::runtime_error("ERROR: path not found!");
}

MyFs::dir_list MyFs::list_dir(std::string path_str)
{
	dir_list ans;
	for (size_t i = 0; i < INODES_COUNT; i++)
	{
		if (nodes.inodes[i].name != std::string(""))
		{
			dir_list_entry entry;
			entry.name = nodes.inodes[i].name;
			entry.file_size = nodes.inodes[i].size;
			entry.is_dir = false;
			ans.push_back(entry);
		}
	}
	return ans;
}

void MyFs::printData()
{
	std::cout << "Nodes: {currNode = " << nodes.currNode << ", currEmptyLoc = " << std::showbase << std::hex << nodes.currEmptyLoc << std::dec << "," << std::endl
			  << "inodes = {" << std::endl;
	for (size_t i = 0; i < INODES_COUNT; i++)
	{
		std::cout << "\tNode " << std::setw(2) << i << std::setw(0) << ": {\'" << nodes.inodes[i].name << "\', " << std::showbase << std::hex << nodes.inodes[i].addr << std::dec << ", " << nodes.inodes[i].size << "}," << std::endl;
	}
	std::cout << "}" << std::endl;
}
